<?php

namespace App\Form;

use App\Entity\Tickets;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TicketType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('probleme', TextType::class , ['required' => true])
            ->add('lieu', TextType::class , ['required' => true])
            ->add('dateecheance', DateType::class, ['required' => true])
            ->add('priorite', ChoiceType::class, [
                'choices'  => [ 
                'normal' =>'ticket-normal' ,
                'haut' => 'ticket-haut',
                'tres-haut' => 'ticket-tres-haut'
                ],
                'required' => true
            ]
            )
            ->add('creer', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Tickets::class,
        ]);
    }
}
