<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Tickets;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TechnicienController extends AbstractController
{
    /**
     * @IsGranted("ROLE_TECHNICIEN")
     */
    /**
     * @Route("/technicien", name="technicien")
     */
    public function index(EntityManagerInterface $entityManager)
    {
        $connection = $entityManager->getConnection();
        $user = $this->getUser();
        $username = $user->getUsername();
        $sql = " SELECT tickets.id , user.username , tickets.probleme , tickets.lieu , tickets.dateecheance , tickets.priorite , tickets.etat , tickets.technicien_affecte  FROM user , tickets where user.id = tickets.user_id_id AND tickets.technicien_affecte = '$username';";
        $statement = $connection->prepare($sql);
        $statement->execute();
        $tickets = $statement->fetchAll();
        return $this->render('technicien/index.html.twig', [
            'tickets' => $tickets,
        ]);
    }
    
    /**
     * @IsGranted("ROLE_TECHNICIEN")
     */
    /**
     * @Route("/avancement", name="avancement")
     */
    public function avancement(Request $request , EntityManagerInterface $entityManager) {

        $avancement = $request->get('avance');
        $id = $request->get('id');
        $tickets = $this->getDoctrine()->getRepository(Tickets::class)->find($id);
        $tickets->setEtat("$avancement");
        $entityManager->merge($tickets);
        $entityManager->flush();
        return new Response("success",200);
    }
}
