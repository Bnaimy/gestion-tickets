<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Tickets;
use Symfony\Component\HttpFoundation\Response;

class CpController extends AbstractController
{
    /**
     * @IsGranted("ROLE_CP")
     */
    /**
     * @Route("/cp", name="cp")
     */
    public function index(EntityManagerInterface $entityManager)
    {
        $connection = $entityManager->getConnection();
        $sql = 'SELECT tickets.id , tickets.technicien_affecte , tickets.probleme , tickets.lieu , tickets.dateecheance , tickets.priorite , tickets.etat , user.username FROM user , tickets where tickets.user_id_id = user.id;';
        $affectation = "SELECT user.id,user.username FROM user where user.roles = '[\"ROLE_TECHNICIEN\"]' ";
        $affectationobject = $connection->prepare($affectation);
        $affectationobject->execute();
        $objectprime = $affectationobject->fetchAll();
        $statement = $connection->prepare($sql);
        $statement->execute();
        $object = $statement->fetchAll();
        return $this->render('cp/index.html.twig', [
            'techniciens' => $objectprime,
            'result' => $object
        ]);
    }
    /**
     * @IsGranted("ROLE_CP")
     */
    /**
     * @Route("/cp", name="cp")
     */
    public function validation(Request $request , EntityManagerInterface $entityManager)
    {
        $id = $request->get('idticket');
        $em = $this->getDoctrine()->getManager();
        $tickets = $this->getDoctrine()->getRepository(Tickets::class)->find($id);
        $tickets->setEtat('Valide');
        $entityManager->merge($tickets);
        $entityManager->flush();
        return new Response("success",200);
    }

    /**
     * @IsGranted("ROLE_CP")
     */
    /**
     * @Route("/affectation_tech", name="tech")
     */
    public function affectation(Request $request , EntityManagerInterface $entityManager) 
    {
        $var1 = $request->get('idticket');
        $var2 = $request->get('tech');
        $tickets = $this->getDoctrine()->getRepository(Tickets::class)->find($var1);
        $tickets->setTechnicienAffecte($var2);
        $entityManager->merge($tickets);
        $entityManager->flush();
        return new Response("success",200);
    }
}
