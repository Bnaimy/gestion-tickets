<?php
namespace App\Controller;
use App\Entity\Tickets;
use App\Form\TicketType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


class ClientController extends AbstractController
{
    /**
     * @IsGranted("ROLE_USER")
     */
    /**
     * @Route("/client", name="client")
     */
    public function index(Request $request)
    {
        $user = $this->getUser();
        $date = "01-09-2015";
        $tickets = $this->getDoctrine()->getRepository(Tickets::class)->findBy(array('user_id'=> $user->getId()));
        $ticket = new Tickets();
        $ticket->setEtat("en cours");
        $ticket->setPriorite("normale");
        $ticket->setDateEcheance(\DateTime::createFromFormat('d-m-Y', $date));
        $ticket->setUserId($this->getUser());
        $form = $this->createForm(TicketType::class, $ticket);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $ticket = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($ticket);
            $entityManager->flush();
            $this->addFlash('ticket', "ticket creer avec succes");
            return $this->redirect('/client');
        }
        $tickets = $this->getDoctrine()->getRepository(Tickets::class)->findBy(array('user_id'=> $user->getId()));
        return $this->render('client/index.html.twig', [
            'tickets' =>$tickets,
            'form' => $form->createView(),
        ]);
    }
    /**
     * @IsGranted("ROLE_USER")
     */
    /**
     * @Route("/remove", name="remove")
     */
    public function remove(Request $request)
    {
            $id = $request->get('idticket');
            $em = $this->getDoctrine()->getManager();
            $tickets = $this->getDoctrine()->getRepository(Tickets::class)->find($id);
            $em->remove($tickets);
            $em->flush();
            return new Response("success",200);
    }

    /**
     * @IsGranted("ROLE_USER")
     */
    public function edit(Request $request, $id)
    {
        $ticket = $this->getDoctrine()->getRepository(Tickets::class)->find($id);
        $form = $this->createFormBuilder($ticket)
        ->add('probleme', TextType::class , ['required' => true])
        ->add('lieu', TextType::class , ['required' => true])
        ->add('dateecheance', DateType::class, ['required' => true])
        ->add('priorite', ChoiceType::class, [
            'choices'  => [ 
            'normal' =>'ticket-normal' ,
            'haut' => 'ticket-haut',
            'tres-haut' => 'ticket-tres-haut'
            ],
            'required' => true
        ]
        )
        ->add('update', SubmitType::class)->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
            return $this->redirectToRoute('client');
        }
        return $this->render('client/update.html.twig', array('form' => $form->createView()));
    }
}
